#include "userprog/process.h"
#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "userprog/gdt.h"
#include "userprog/pagedir.h"
#include "userprog/tss.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "filesys/inode.h"
#include "threads/flags.h"
#include "threads/init.h"
#include "threads/interrupt.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/malloc.h"
#include "vm/spage.h"


static thread_func start_process NO_RETURN;
static bool load (const char *cmdline, void (**eip) (void), void **esp);
static void compress_spaces(char *str);
struct PCB* get_PCB_child (struct PCB *parent, pid_t pid);
    
/* Synchronization with 'syscall.c' filesystem syscalls. */
extern struct lock file_sync;

/* Synchronization for process_execute() and start_process(). */
static struct PCB *loading_PCB;

/* Create the PCB for the initial thread. */
void 
process_init (void)
{
    struct PCB *pcb;

    /* Create a PCB for the init thread. */
    pcb = malloc(sizeof(struct PCB));

    pcb->pid = 1;
    pcb->parent_pcb = NULL;
    pcb->exfile = NULL;
    pcb->status = PROC_LIVING;
    pcb->exit_status = EXIT_FAILURE;
    pcb->fd_count = STDIN_FILENO+1;

    list_init (&pcb->fd_list);
    list_init (&pcb->child_list);

    /* Add a pointer to the PCB in the thread structure. */
    thread_current ()->pcb = pcb;
}

/* Starts a new thread running a user program loaded from
   FILENAME.  The new thread may be scheduled (and may even exit)
   before process_execute() returns.  Returns the new process's
   thread id, or TID_ERROR if the thread cannot be created. */
tid_t
process_execute (const char *file_name) 
{
  char *fn_copy, *exec_file;
  struct PCB *pcb = NULL;
  size_t fsize=0;

  /* Make two copies of FILE_NAME.
     Otherwise there's a race between the caller and load(). */
  fn_copy = palloc_get_page (0);
  exec_file = palloc_get_page (0);
  if (fn_copy == NULL || exec_file == NULL)
    return TID_ERROR;
  
  strlcpy (fn_copy, file_name, PGSIZE);

  /* Get only the file name of the executable. */
  while (file_name[fsize] != ' ' ) fsize++;
  strlcpy (exec_file, file_name, fsize+1);

  /* Create a new Process Control Block. */
  pcb = malloc (sizeof(struct PCB));
  if (pcb == NULL) return PID_ERROR;

  /* Ensure synchronization with start_process () function. */
  sema_init (&pcb->load, 0);
  sema_init (&pcb->run, 0);
  loading_PCB = pcb;
  spage_init (pcb);

  /* Create a new thread to execute FILE_NAME. */
  pcb->pid = thread_create (exec_file, PRI_DEFAULT, start_process, fn_copy);
  palloc_free_page (exec_file);

  if (pcb->pid == TID_ERROR)
  {
    free (pcb);
    palloc_free_page (fn_copy);
    return PID_ERROR;
  }

  /* Wait for the process to load. */
  sema_down (&pcb->load);

  /* Check if the process has been correctly loaded. */
  if (pcb->status == PROC_ERROR)
  {
      free(pcb);
      return PID_ERROR;
  }

  /* Finish to initialize the PCB. */
  pcb->parent_pcb = thread_current ()->pcb;
  pcb->status = PROC_LIVING;
  pcb->exit_status = EXIT_FAILURE;
  pcb->fd_count = STDOUT_FILENO+1;
  list_init (&pcb->fd_list);
  list_init (&pcb->child_list);
  sema_init (&pcb->sema, 0);

  /* Add the new created child to our children list. */
  list_push_front (&pcb->parent_pcb->child_list, &pcb->elem);

  /* Tell to the child thread that it can run. */
  sema_up (&pcb->run);

  return pcb->pid;
}

/* A thread function that loads a user process and makes it start
   running. */
static void
start_process (void *f_name)
{
  char *file_name = f_name;
  struct intr_frame if_;
  bool success;
  struct PCB *pcb = loading_PCB;
  struct thread *t = thread_current ();
  
  /* Assign the PCB to the new thread. */
  t->pcb = pcb;

  /* Initialize interrupt frame and load executable. */
  memset (&if_, 0, sizeof if_);
  if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
  if_.cs = SEL_UCSEG;
  if_.eflags = FLAG_IF | FLAG_MBS;
  success = load (file_name, &if_.eip, &if_.esp);
  palloc_free_page (file_name);

  /* Signal to the parent that it can read if load success or not. */
  if (success)
  {
      /* Mark the file as read only. */
      lock_acquire (&file_sync);
        pcb->exfile = filesys_open (t->name);
        file_deny_write (pcb->exfile);
      lock_release (&file_sync);

      /* Send loading signal to process_execute (). */
      pcb->status = PROC_LOADED;
      sema_up (&pcb->load);
  }
  else
  {
    /* Send loading signal to process_execute (). */
    pcb->status = PROC_ERROR;  
    sema_up (&pcb->load);

    thread_exit ();
    NOT_REACHED ();
  }

  /* Wait for PCB initialization, and start the process. */
  sema_down (&pcb->run);

  /* Start the user process by simulating a return from an
     interrupt, implemented by intr_exit (in
     threads/intr-stubs.S).  Because intr_exit takes all of its
     arguments on the stack in the form of a `struct intr_frame',
     we just point the stack pointer (%esp) to our stack frame
     and jump to it. */
  asm volatile ("movl %0, %%esp; jmp intr_exit" : : "g" (&if_) : "memory");
  NOT_REACHED ();
}

/* Waits for thread TID to die and returns its exit status.  If
   it was terminated by the kernel (i.e. killed due to an
   exception), returns -1.  If TID is invalid or if it was not a
   child of the calling process, or if process_wait() has already
   been successfully called for the given TID, returns -1
   immediately, without waiting.

   This function will be implemented in problem 2-2.  For now, it
   does nothing. */
int
process_wait (tid_t child_tid) 
{
    int exit_status = -1;
    struct PCB *pcb, *child_pcb;

    /* Search if the process with the pid exists in process_list. */
    pcb = thread_current ()->pcb;
    child_pcb = get_PCB_child (pcb, child_tid);

    /* If we cannot find the PCB, it could mean : 
     * 1. the calling process does not own the child with 'child_tid'
     * 2. child has been killed 
     * 3. it is the second time that process_wait() is called with this PID 
     * number. */
    if (child_pcb == NULL)
        return -1;

    /* Check if the child is living. */
    switch (child_pcb->status)
    {
            /* Child process is living, or waiting for another child. 
            * We should wait for the child to call exit() syscall, or
            * O/S killing. */
        case PROC_LIVING : case PROC_WAITING :
            pcb->status = PROC_WAITING;
            sema_down (&child_pcb->sema);
            pcb->status = PROC_LIVING;
            ASSERT (child_pcb->status == PROC_EXITED)

            /* Child has already exited in the past. Remove and destruct the
             * child's PCB, remove it from our children list, and return the 
             * exit number. */
        case PROC_EXITED :
            exit_status = child_pcb->exit_status;
            list_remove (&child_pcb->elem);
            free (child_pcb);
            break;

        default :
            PANIC ("PCB corruption of thread with pid %d", child_pcb->pid);
            break;
    }

    return exit_status;
}

/* This function look among all processes in the child_list of the PCB provided
 * in parameter, and try to find if a process match with 'pid'. If found, return 
 * the PCB structure. If not, return NULL pointer. */
struct PCB*
get_PCB_child (struct PCB *parent, pid_t pid)
{
    struct list_elem *e;
    struct PCB *p;

    /* Browse the children list. */
    for (e = list_begin (&parent->child_list); e != list_end (&parent->child_list);
            e = list_next (e))
    {
        /* Get the enclosing structure. */
        p = list_entry (e, struct PCB, elem);

        /* Check if the PCB is not corrupted. */
        ASSERT (p->parent_pcb == parent);

        if (p->pid == pid)
            return p;
    }

    /* If we reach this point, it means that the PID hasn't been found in the
     * children list. Return NULL. */
    return NULL;
}

/* Function called from 'exception.c'. Wait all child process to finish, and
 * finally terminate the calling process. */
void
process_kill (struct PCB *parent)
{
    pid_t cpid;

    /* Wait for all children to die. */
    while (!list_empty (&parent->child_list))
    {
        /* Get the first element in the child_list. */
        cpid = list_entry (
                list_begin (&parent->child_list), struct PCB, elem)->pid;

        process_wait (cpid);
    }

    exit (-1);
}

/* Free the current process's resources. */
void
process_exit (void)
{
  struct thread *curr = thread_current ();
  uint32_t *pd;

  /* Destroy the current process's page directory and switch back
     to the kernel-only page directory. */
  pd = curr->pagedir;
  if (pd != NULL) 
    {
      /* Correct ordering here is crucial.  We must set
         cur->pagedir to NULL before switching page directories,
         so that a timer interrupt can't switch back to the
         process page directory.  We must activate the base page
         directory before destroying the process's page
         directory, or our active page directory will be one
         that's been freed (and cleared). */
      curr->pagedir = NULL;
      pagedir_activate (NULL);
      pagedir_destroy (pd);
    }
}

/* Sets up the CPU for running user code in the current
   thread.
   This function is called on every context switch. */
void
process_activate (void)
{
  struct thread *t = thread_current ();

  /* Activate thread's page tables. */
  pagedir_activate (t->pagedir);

  /* Set thread's kernel stack for use in processing
     interrupts. */
  tss_update ();
}

/* We load ELF binaries.  The following definitions are taken
   from the ELF specification, [ELF1], more-or-less verbatim.  */

/* ELF types.  See [ELF1] 1-2. */
typedef uint32_t Elf32_Word, Elf32_Addr, Elf32_Off;
typedef uint16_t Elf32_Half;

/* For use with ELF types in printf(). */
#define PE32Wx PRIx32   /* Print Elf32_Word in hexadecimal. */
#define PE32Ax PRIx32   /* Print Elf32_Addr in hexadecimal. */
#define PE32Ox PRIx32   /* Print Elf32_Off in hexadecimal. */
#define PE32Hx PRIx16   /* Print Elf32_Half in hexadecimal. */

/* Executable header.  See [ELF1] 1-4 to 1-8.
   This appears at the very beginning of an ELF binary. */
struct Elf32_Ehdr
  {
    unsigned char e_ident[16];
    Elf32_Half    e_type;
    Elf32_Half    e_machine;
    Elf32_Word    e_version;
    Elf32_Addr    e_entry;
    Elf32_Off     e_phoff;
    Elf32_Off     e_shoff;
    Elf32_Word    e_flags;
    Elf32_Half    e_ehsize;
    Elf32_Half    e_phentsize;
    Elf32_Half    e_phnum;
    Elf32_Half    e_shentsize;
    Elf32_Half    e_shnum;
    Elf32_Half    e_shstrndx;
  };

/* Program header.  See [ELF1] 2-2 to 2-4.
   There are e_phnum of these, starting at file offset e_phoff
   (see [ELF1] 1-6). */
struct Elf32_Phdr
  {
    Elf32_Word p_type;
    Elf32_Off  p_offset;
    Elf32_Addr p_vaddr;
    Elf32_Addr p_paddr;
    Elf32_Word p_filesz;
    Elf32_Word p_memsz;
    Elf32_Word p_flags;
    Elf32_Word p_align;
  };

/* Values for p_type.  See [ELF1] 2-3. */
#define PT_NULL    0            /* Ignore. */
#define PT_LOAD    1            /* Loadable segment. */
#define PT_DYNAMIC 2            /* Dynamic linking info. */
#define PT_INTERP  3            /* Name of dynamic loader. */
#define PT_NOTE    4            /* Auxiliary info. */
#define PT_SHLIB   5            /* Reserved. */
#define PT_PHDR    6            /* Program header table. */
#define PT_STACK   0x6474e551   /* Stack segment. */

/* Flags for p_flags.  See [ELF3] 2-3 and 2-4. */
#define PF_X 1          /* Executable. */
#define PF_W 2          /* Writable. */
#define PF_R 4          /* Readable. */

static bool setup_stack (void **esp);
static bool validate_segment (const struct Elf32_Phdr *, struct file *);
static bool load_segment (struct file *file, off_t ofs, uint8_t *upage,
                          uint32_t read_bytes, uint32_t zero_bytes,
                          bool writable);
static bool argpass (const char* arguments, void **esp);

/* Parse all arguments from file_name and put them on the stack 
 * according to the 80x86 convention. */
bool 
argpass (const char* arguments, void **esp)
{
    size_t argl, word_align, cnt;
    char *pch, *save_ptr, *cmd_arg;
    uint32_t* ptr32, *argtab_ptr;
    int i;

    /* Make a copy of the name, as strtok_r is destructive. */
    argl = strlen(arguments);
    cmd_arg = (char*) calloc (argl+1, sizeof(char));
    if (cmd_arg == NULL) return false;
    strlcpy (cmd_arg, arguments, argl+1);

    /* Remove extra spaces in the cmd_arg. */
    compress_spaces (cmd_arg);

    argl = strlen (cmd_arg);

    /* Get the number of argument counting number of spaces. */
    for (i=0, cnt=1; cmd_arg[i]; i++)
        cnt += (cmd_arg[i] == ' ');

    /* Create an array that stores each parsed arguments. */
    argtab_ptr = (uint32_t*) calloc (cnt, sizeof(uint32_t*));
    if(argtab_ptr == NULL) return false;

    /* Calculate word alignement. */
    word_align = ((uint32_t)*esp - (argl+1))%4;

    /* Initialize strtok_r for parsing. */
    pch = strtok_r ((char*) cmd_arg, " ", &save_ptr);
    i=0;

    while(pch != NULL)
    {
        /* Calculate the new position of the stack pointer. */
        *esp = *esp-strlen(pch)-1;
        memcpy (*esp, pch, strlen(pch)+1);

        /* Store current pointer into the arg_tab. */
        argtab_ptr[i++] = (uint32_t) *esp;

        /* Parse the next argument. */
        pch = strtok_r (NULL, " ", &save_ptr);
    }

    /* Proceed to alignement. */
    ptr32 = (uint32_t*)(*esp - word_align);

    /* Add the sentinel argument. */
    ptr32--;

    /* Add the arguments addresses. */
    for(i=1; i<=(int)cnt; i++)
        *(--ptr32) = argtab_ptr[cnt-i];

    /* Add the address of the beginning of arguments addresses. */
    --ptr32;
    *ptr32 = (uint32_t) (ptr32 + 1);

    /* Add number of arguments. */
    *(--ptr32) = (int) cnt;
    *(--ptr32) = (uint32_t) 0x00;

    /* Restore esp pointer. */
    *esp = ptr32;

    /* Free ressources. */
    free(argtab_ptr);
    free(cmd_arg);

    return true;
}
/* Loads an ELF executable from FILE_NAME into the current thread.
   Stores the executable's entry point into *EIP
   and its initial stack pointer into *ESP.
   Returns true if successful, false otherwise. */
bool
load (const char *file_name, void (**eip) (void), void **esp) 
{
  struct thread *t = thread_current ();
  struct Elf32_Ehdr ehdr;
  struct file *file = NULL;
  off_t file_ofs;
  bool success = false;
  int i;

  /* Allocate and activate page directory. */
  t->pagedir = pagedir_create ();
  if (t->pagedir == NULL) 
    goto done;
  process_activate ();

  /* Open executable file. */
  file = filesys_open (t->name);
  if (file == NULL) 
    {
      printf ("load: %s: open failed\n", file_name);
      goto done; 
    }

  /* Read and verify executable header. */
  if (file_read (file, &ehdr, sizeof ehdr) != sizeof ehdr
      || memcmp (ehdr.e_ident, "\177ELF\1\1\1", 7)
      || ehdr.e_type != 2
      || ehdr.e_machine != 3
      || ehdr.e_version != 1
      || ehdr.e_phentsize != sizeof (struct Elf32_Phdr)
      || ehdr.e_phnum > 1024) 
    {
      printf ("load: %s: error loading executable\n", file_name);
      goto done; 
    }

  /* Read program headers. */
  file_ofs = ehdr.e_phoff;
  for (i = 0; i < ehdr.e_phnum; i++) 
    {
      struct Elf32_Phdr phdr;

      if (file_ofs < 0 || file_ofs > file_length (file))
        goto done;
      file_seek (file, file_ofs);

      if (file_read (file, &phdr, sizeof phdr) != sizeof phdr)
        goto done;
      file_ofs += sizeof phdr;
      switch (phdr.p_type) 
        {
        case PT_NULL:
        case PT_NOTE:
        case PT_PHDR:
        case PT_STACK:
        default:
          /* Ignore this segment. */
          break;
        case PT_DYNAMIC:
        case PT_INTERP:
        case PT_SHLIB:
          goto done;
        case PT_LOAD:
          if (validate_segment (&phdr, file)) 
            {
              bool writable = (phdr.p_flags & PF_W) != 0;
              uint32_t file_page = phdr.p_offset & ~PGMASK;
              uint32_t mem_page = phdr.p_vaddr & ~PGMASK;
              uint32_t page_offset = phdr.p_vaddr & PGMASK;
              uint32_t read_bytes, zero_bytes;
              if (phdr.p_filesz > 0)
                {
                  /* Normal segment.
                     Read initial part from disk and zero the rest. */
                  read_bytes = page_offset + phdr.p_filesz;
                  zero_bytes = (ROUND_UP (page_offset + phdr.p_memsz, PGSIZE)
                                - read_bytes);
                }
              else 
                {
                  /* Entirely zero.
                     Don't read anything from disk. */
                  read_bytes = 0;
                  zero_bytes = ROUND_UP (page_offset + phdr.p_memsz, PGSIZE);
                }
              if (!load_segment (file, file_page, (void *) mem_page,
                                 read_bytes, zero_bytes, writable))
                goto done;
            }
          else
            goto done;
          break;
        }
    }

  /* Set up stack. */
  if (!setup_stack (esp))
    goto done;

  /* Parse arguments and fill the stack. */
  argpass (file_name, esp);
  
  /* Start address. */
  *eip = (void (*) (void)) ehdr.e_entry;

  success = true;

 done:
  /* We arrive here whether the load is successful or not. */
  file_close (file);
  return success;
}

/* Remove multiple spaces and form only one space from a string. 
 * Credits: 
   http://stackoverflow.com/questions/1217721/
   how-do-i-replace-multiple-spaces-with-a-single-space*/
void 
compress_spaces(char *str)
{
    char *dst = str;

    for (; *str; ++str) {
        *dst++ = *str;

        if (*str == ' ') {
            do ++str; 
            while (*str == ' ');
            --str;
        }
    }
    /* Protect against end-space terminated string. */
    if ( *(dst-1) == ' ')
        *(dst-1) = 0;
    else
        *dst = 0;
}



/* Checks whether PHDR describes a valid, loadable segment in
   FILE and returns true if so, false otherwise. */
static bool
validate_segment (const struct Elf32_Phdr *phdr, struct file *file) 
{
  /* p_offset and p_vaddr must have the same page offset. */
  if ((phdr->p_offset & PGMASK) != (phdr->p_vaddr & PGMASK)) 
    return false; 

  /* p_offset must point within FILE. */
  if (phdr->p_offset > (Elf32_Off) file_length (file)) 
    return false;

  /* p_memsz must be at least as big as p_filesz. */
  if (phdr->p_memsz < phdr->p_filesz) 
    return false; 

  /* The segment must not be empty. */
  if (phdr->p_memsz == 0)
    return false;
  
  /* The virtual memory region must both start and end within the
     user address space range. */
  if (!is_user_vaddr ((void *) phdr->p_vaddr))
    return false;
  if (!is_user_vaddr ((void *) (phdr->p_vaddr + phdr->p_memsz)))
    return false;

  /* The region cannot "wrap around" across the kernel virtual
     address space. */
  if (phdr->p_vaddr + phdr->p_memsz < phdr->p_vaddr)
    return false;

  /* Disallow mapping page 0.
     Not only is it a bad idea to map page 0, but if we allowed
     it then user code that passed a null pointer to system calls
     could quite likely panic the kernel by way of null pointer
     assertions in memcpy(), etc. */
  if (phdr->p_vaddr < PGSIZE)
    return false;

  /* It's okay. */
  return true;
}

/* Loads a segment starting at offset OFS in FILE at address
   UPAGE.  In total, READ_BYTES + ZERO_BYTES bytes of virtual
   memory are initialized, as follows:

        - READ_BYTES bytes at UPAGE must be read from FILE
          starting at offset OFS.

        - ZERO_BYTES bytes at UPAGE + READ_BYTES must be zeroed.

   The pages initialized by this function must be writable by the
   user process if WRITABLE is true, read-only otherwise.

   Return true if successful, false if a memory allocation error
   or disk read error occurs. */
static bool
load_segment (struct file *file, off_t ofs, uint8_t *upage,
              uint32_t read_bytes, uint32_t zero_bytes, bool writable) 
{
  ASSERT ((read_bytes + zero_bytes) % PGSIZE == 0);
  ASSERT (pg_ofs (upage) == 0);
  ASSERT (ofs % PGSIZE == 0);

  file_seek (file, ofs);
  while (read_bytes > 0 || zero_bytes > 0) 
    {
      /* Do calculate how to fill this page.
         We will read PAGE_READ_BYTES bytes from FILE
         and zero the final PAGE_ZERO_BYTES bytes. */
      size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
      size_t page_zero_bytes = PGSIZE - page_read_bytes;

      /* Set the proper flag, and and prepare the OS to load segment lazily. */
      uint8_t flag = 0x00;
      if (writable)
          flag = FRM_SEGWR;
      else
          flag = FRM_SEGRO;

      flag |= LOC_LAZY;
      flag |= LOC_EVICTED;

      disk_sector_t sec = byte_to_sector (file_get_inode (file), 
              file_tell(file));

      /* Allocate lazily : no frame will be get from the pool, but add the 
       * segment to the evicted page of the current process. */
      spage_allocate (upage, flag, sec, page_zero_bytes);

      /* Advance. */
      ofs += page_read_bytes;
      file_seek (file, ofs);

      read_bytes -= page_read_bytes;
      zero_bytes -= page_zero_bytes;
      upage += PGSIZE;
    }
  return true;
}

/* Create a minimal stack by mapping a zeroed page at the top of
   user virtual memory. */
static bool
setup_stack (void **esp) 
{
  spage_allocate (((uint8_t *) PHYS_BASE) - PGSIZE, 
          FRM_STACK|LOC_MEM, 0, 0);

  *esp = PHYS_BASE;

  return true;
}

/* Adds a mapping from user virtual address UPAGE to kernel
   virtual address KPAGE to the page table.
   If WRITABLE is true, the user process may modify the page;
   otherwise, it is read-only.
   UPAGE must not already be mapped.
   KPAGE should probably be a page obtained from the user pool
   with palloc_get_page().
   Returns true on success, false if UPAGE is already mapped or
   if memory allocation fails. */
bool
install_page (void *upage, void *kpage, bool writable)
{
  struct thread *t = thread_current ();

  /* Verify that there's not already a page at that virtual
     address, then map our page there. */
  return (pagedir_get_page (t->pagedir, upage) == NULL
          && pagedir_set_page (t->pagedir, upage, kpage, writable));
}

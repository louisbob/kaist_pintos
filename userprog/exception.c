#include "userprog/exception.h"
#include <inttypes.h>
#include <stdio.h>
#include "userprog/gdt.h"
#include "userprog/pagedir.h"
#include "userprog/process.h"
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "vm/spage.h"

/* Number of page faults processed. */
static long long page_fault_cnt;

/* Global variable for pointer-check. */
static uint32_t ptr_check;

static void kill (struct intr_frame *);
static void page_fault (struct intr_frame *);

/* Registers handlers for interrupts that can be caused by user
   programs.

   In a real Unix-like OS, most of these interrupts would be
   passed along to the user process in the form of signals, as
   described in [SV-386] 3-24 and 3-25, but we don't implement
   signals.  Instead, we'll make them simply kill the user
   process.

   Page faults are an exception.  Here they are treated the same
   way as other exceptions, but this will need to change to
   implement virtual memory.

   Refer to [IA32-v3a] section 5.15 "Exception and Interrupt
   Reference" for a description of each of these exceptions. */
void
exception_init (void) 
{
  /* These exceptions can be raised explicitly by a user program,
     e.g. via the INT, INT3, INTO, and BOUND instructions.  Thus,
     we set DPL==3, meaning that user programs are allowed to
     invoke them via these instructions. */
  intr_register_int (3, 3, INTR_ON, kill, "#BP Breakpoint Exception");
  intr_register_int (4, 3, INTR_ON, kill, "#OF Overflow Exception");
  intr_register_int (5, 3, INTR_ON, kill,
                     "#BR BOUND Range Exceeded Exception");

  /* These exceptions have DPL==0, preventing user processes from
     invoking them via the INT instruction.  They can still be
     caused indirectly, e.g. #DE can be caused by dividing by
     0.  */
  intr_register_int (0, 0, INTR_ON, kill, "#DE Divide Error");
  intr_register_int (1, 0, INTR_ON, kill, "#DB Debug Exception");
  intr_register_int (6, 0, INTR_ON, kill, "#UD Invalid Opcode Exception");
  intr_register_int (7, 0, INTR_ON, kill,
                     "#NM Device Not Available Exception");
  intr_register_int (11, 0, INTR_ON, kill, "#NP Segment Not Present");
  intr_register_int (12, 0, INTR_ON, kill, "#SS Stack Fault Exception");
  intr_register_int (13, 0, INTR_ON, kill, "#GP General Protection Exception");
  intr_register_int (16, 0, INTR_ON, kill, "#MF x87 FPU Floating-Point Error");
  intr_register_int (19, 0, INTR_ON, kill,
                     "#XF SIMD Floating-Point Exception");

  /* Most exceptions can be handled with interrupts turned on.
     We need to disable interrupts for page faults because the
     fault address is stored in CR2 and needs to be preserved. */
  intr_register_int (14, 0, INTR_OFF, page_fault, "#PF Page-Fault Exception");
  
  /* Initialize the check pointer. */
  ptr_check = (uint32_t) PHYS_BASE;
}

/* Prints exception statistics. */
void
exception_print_stats (void) 
{
  printf ("Exception: %lld page faults\n", page_fault_cnt);
}

/* Handler for an exception (probably) caused by a user process. */
static void
kill (struct intr_frame *f) 
{
  /* This interrupt is one (probably) caused by a user process.
     For example, the process might have tried to access unmapped
     virtual memory (a page fault).  For now, we simply kill the
     user process.  Later, we'll want to handle page faults in
     the kernel.  Real Unix-like operating systems pass most
     exceptions back to the process via signals, but we don't
     implement them. */
     
  /* The interrupt frame's code segment value tells us where the
     exception originated. */
  switch (f->cs)
    {
    case SEL_UCSEG:
      /* User's code segment, so it's a user exception, as we
         expected.  Kill the user process.  */
      printf ("%s: dying due to interrupt %#04x (%s).\n",
              thread_name (), f->vec_no, intr_name (f->vec_no));
      intr_dump_frame (f);

      /* Wait for all our children to exit before killing this process. */
      process_kill (thread_current ()->pcb);

    case SEL_KCSEG:
      /* Kernel's code segment, which indicates a kernel bug.
         Kernel code shouldn't throw exceptions.  (Page faults
         may cause kernel exceptions--but they shouldn't arrive
         here.)  Panic the kernel to make the point.  */
      intr_dump_frame (f);
      PANIC ("Kernel bug - unexpected interrupt in kernel"); 

    default:
      /* Some other code segment?  Shouldn't happen.  Panic the
         kernel. */
      printf ("Interrupt %#04x (%s) in unknown segment %04x\n",
             f->vec_no, intr_name (f->vec_no), f->cs);

      /* Wait for all our children to exit before killing this process. */
      process_kill (thread_current ()->pcb);
    }
}

/* Page fault handler.  This is a skeleton that must be filled in
   to implement virtual memory.  Some solutions to project 2 may
   also require modifying this code.

   At entry, the address that faulted is in CR2 (Control Register
   2) and information about the fault, formatted as described in
   the PF_* macros in exception.h, is in F's error_code member.  The
   example code here shows how to parse that information.  You
   can find more information about both of these in the
   description of "Interrupt 14--Page Fault Exception (#PF)" in
   [IA32-v3a] section 5.15 "Exception and Interrupt Reference". */
static void
page_fault (struct intr_frame *f) 
{
  bool not_present;  /* True: not-present page, false: writing r/o page. */
  bool write;        /* True: access was write, false: access was read. */
  bool user;         /* True: access by user, false: access by kernel. */
  void *fault_addr;  /* Fault address. */
  struct thread *t = thread_current ();

  /* Obtain faulting address, the virtual address that was
     accessed to cause the fault.  It may point to code or to
     data.  It is not necessarily the address of the instruction
     that caused the fault (that's f->eip).
     See [IA32-v2a] "MOV--Move to/from Control Registers" and
     [IA32-v3a] 5.15 "Interrupt 14--Page Fault Exception
     (#PF)". */
  asm ("movl %%cr2, %0" : "=r" (fault_addr));

  /* Turn interrupts back on (they were only off so that we could
     be assured of reading CR2 before it changed). */
  intr_enable ();

  /* Check if the page_fault is caused by check_ptr_validity(). */
  if (ptr_check != (uint32_t)PHYS_BASE)
  {
      if (ptr_check == (uint32_t)fault_addr)
      {
          /* By observing intr_frame structure, EIP point to the next 
           * instruction to execute. As we don't want to reproduce a page_fault,
           * we should increment EIP in order to execute the instruction after 
           * our pointer check. To determine how much we should increment EIP, we
           * can see that each code segment is 16bits long. Incrementing f->eip 
           * of 2 bytes should point to the next instruction, as the instruction 
           * we used for generating this page_fault is atomic. */
          f->eip = f->eip + 2;

          /* Restore the check pointer. */
          ptr_check = (uint32_t) PHYS_BASE; 
          return;
      }
  }  

  /* Count page faults. */
  page_fault_cnt++;

  /* Determine cause. */
  not_present = (f->error_code & PF_P) == 0;
  write = (f->error_code & PF_W) != 0;
  user = (f->error_code & PF_U) != 0;

   /* Check if the page fault is due after a page eviction. */
  if (not_present && spage_fetch_evicted (fault_addr))
  {
      return;
  }

  /* Check if we need to grow the stack. We should check that the address is 
   * not trying to grown the stack below the stack limit (8MB). */
  if (write && user && not_present
          && fault_addr >= (void*) STACK_LIM 
          && fault_addr < PHYS_BASE)
  {
      /* Consider that this fault is a stack growth. */
      uint8_t *vpage = pg_round_down (fault_addr);

      /* Allocate frame until reaching the current stack pointer. */
      while (pagedir_get_page (t->pagedir, vpage) == NULL)
      {
          /* Allocate a frame for the stack. */
          spage_allocate (vpage, FRM_STACK|LOC_MEM, 0, 0);

          /* Increment the vpage addr. */
          vpage += PGSIZE;
      }
      return;
  }
 
  kill(f);

  NOT_REACHED ();
  /* To implement virtual memory, delete the rest of the function
     body, and replace it with code that brings in the page to
     which fault_addr refers. */
  printf ("Page fault at %p: %s error %s page in %s context.\n",
          fault_addr,
          not_present ? "not present" : "rights violation",
          write ? "writing" : "reading",
          user ? "user" : "kernel");
}

/* Check if the pointer provided in parameter is owned by the callee process.
 * This function first checks if the pointer's address is in kernel space
 * then try to generates a handleable page_fault exception to detect if the
 * pointer's address belong to another process page. 
 * The second parameter, 'write', specify if something will be written at the 
 * specified address. If yes, this function also check if the page that will 
 * be written is Read Only or not. 
 * Return true if the address is valid, false otherwise. It takes advantage 
 * of the MMU. */
bool
check_ptr_validity (void* vaddr, bool write)
{
    uint32_t *ptr = (uint32_t*) vaddr;

    /* 'volatile' type prevent compiler to optimize this pointer by deleting 
     * it. */
    volatile uint32_t p2;

    /* Check if the pointer point to kernel memory pool. */
    if (!is_user_vaddr (vaddr))
        return false;

    /* Prepare the page_fault exception to handle our pointer checking, in the
     * case of deferencing the pointer trigger a page_fault exception. */
    ptr_check = (uint32_t) ptr;

    /* Deferencing the pointer. */
    p2 = *(uint32_t*)ptr_check;

    /* If a page_fault occured, ptr_check should have been modified by the 
     * page_fault exception handler. */
    if (ptr_check != (uintptr_t) vaddr)
    {
        /* Try to fetch the evicted page. If no page is found, return false. */
        if(!spage_fetch_evicted (vaddr))
            return false;
    }

    /* Check if the page of the pointer is read only. */
    if (write)
    {
        struct thread *t = thread_current ();
        if(!pagedir_is_writable(t->pagedir, pg_round_down(vaddr)))
        {
            return false;
        }
    }

    return true;
}



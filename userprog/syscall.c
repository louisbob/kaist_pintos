#include "filesys/file.h"
#include "filesys/filesys.h"
#include "filesys/inode.h"
#include "filesys/off_t.h"
#include "devices/input.h"
#include "lib/user/syscall.h"
#include "userprog/exception.h"
#include "userprog/syscall.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include <stdio.h>
#include <string.h>
#include <syscall-nr.h>
#include "threads/malloc.h"
#include "threads/init.h"
#include "threads/interrupt.h"
#include "threads/palloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "vm/spage.h"
#include "vm/frame.h"
#include "vm/swap.h"

static void syscall_handler (struct intr_frame *f UNUSED);
static void empty_fdlist (struct PCB *p);
static mapid_t mapfd_create (struct file_d *fd, void* vaddr);
static void mapfd_close (struct file_d *fd);

/* Synchronization for filesystem syscalls. */
struct lock file_sync;

extern struct frame_hl frame_list;

void 
syscall_init (void)
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");

  /* Initialize the list of all thread that run(ned) on the system. */
  process_init();

  /* Initialize synchronization for read/write operations. */
  lock_init (&file_sync);
}

static void 
syscall_handler (struct intr_frame *f UNUSED)
{
    int syscall_no;
    void **atab;

    /* Check if the stack pointer is valid, and check if the stack pointer has 
     * a logical position. As there is always a syscall number in the stack, 
     * check if the syscall number is not over the pool. */
    if (!check_ptr_validity ((void*)f->esp, false))
         exit(-1);

    /* Pin the stack page to avoid eviction of it by another process. */
    struct spage *pg = frame_pin (&frame_list, f->esp);

    /* Get syscall number from the stack. */
    syscall_no = *((int*)f->esp);

    /* Check if there is no error in the number of argument. To do that, 
     * calculate the top address of the syscall stack, and check whether or 
     * not we get an invalid pointer. */
    if (!check_ptr_validity ((void*)(f->esp+4*syscall_no), false))
        exit(-1);
    
    /* Initializing the argument table with pointers to arguments on the stack.
     * (C99 compliant declaration). */
    atab = (void* [3]) {f->esp + 4, f->esp + 8, f->esp + 12};

    /* Function call is done depending on the syscall number. */
    switch (syscall_no)
    {
        case  SYS_HALT :           halt ();
                 break; 
        case  SYS_EXIT :           exit (*(int*)atab[0]);
                 break;
        case  SYS_EXEC :     f->eax = exec (*(const char**) atab[0]);
                 break;
        case  SYS_WAIT :     f->eax = wait (*(int*) atab[0]);
                 break;
        case  SYS_CREATE :   f->eax = create (*(const char**) atab[0],*(int*) atab[1]);
                 break;
        case  SYS_REMOVE :   f->eax = remove (*(const char**) atab[0]);
                 break;
        case  SYS_OPEN :     f->eax = open (*(const char**) atab[0]);
                 break;
        case  SYS_FILESIZE : f->eax = filesize (*(int*) atab[0]);
                 break;
        case  SYS_READ :     f->eax = read (*(int*) atab[0],*(void **) atab[1],
                                     *(int*) atab[2]);
                 break;
        case  SYS_WRITE :    f->eax = write (*(int*) atab[0],*(void **) atab[1],
                                     *(int*) atab[2]);
                 break;
        case  SYS_SEEK :           seek (*(int*) atab[0],*(int*) atab[1]);
                 break;
        case  SYS_TELL :     f->eax = tell (*(int*) atab[0]);
                 break;
        case  SYS_CLOSE :          close (*(int*) atab[0]);
                 break;
        case SYS_MMAP :      f->eax = mmap (*(int*) atab[0], *(void **) atab[1]);
                 break;
        case SYS_MUNMAP :          munmap (*(mapid_t*) atab[0]);
                 break;
    }

    /* Unpin the stack page. */
    frame_unpin (&frame_list, pg);
}

/* -=| USEFUL FUNCTIONS DEFINITION FOR SYSCALL |=-*/

/* Retrieve the file descriptor structure of the calling thread, using 
 * the file descriptor number in pamarameter. Return NULL if not found. */
struct file_d*
process_search_fd (int fd)
{
    struct list_elem *e;
    struct PCB *pcb = thread_current ()->pcb;

    ASSERT (pcb != NULL);

    for (e = list_begin (&pcb->fd_list); e != list_end (&pcb->fd_list);
            e = list_next (e))
    {
        if (list_entry (e, struct file_d, elem)->fd_no == fd)
            return list_entry (e, struct file_d, elem);
    }

    return NULL;
}

/* Go throught the file descriptor list of the current PCB and close all
 * of them. */
void
empty_fdlist (struct PCB *p)
{
    struct file_d *fd;

    ASSERT (p != NULL)

    while (!list_empty (&p->fd_list))
    {
        /* Pop the file descriptor from the list of the PCB. */
        fd = list_entry (list_pop_front (&p->fd_list), struct file_d, elem);

        /* Close the file descriptor. */
        file_close (fd->file);

        /* Free the file descriptor. */
        free (fd);
    }
}

mapid_t 
mapfd_create (struct file_d *fd, void* vaddr)
{
    struct PCB *pcb = thread_current ()->pcb;
    struct file* f;
    struct file_d *nfd;

    /* Try to reopen the file. */
    f = file_reopen (fd->file);

    /* Create a new file descriptor structure. */
    nfd = (struct file_d*) malloc (sizeof(struct file_d));
    ASSERT (nfd != NULL);

    /* Fill it. */
    nfd->fd_no = pcb->fd_count++;
    nfd->file = f;
    nfd->pos = 0;
    nfd->mmap_addr = vaddr;
    list_push_front (&pcb->fd_list, &nfd->elem);

    return nfd->fd_no;

}

void mapfd_close (struct file_d *fd)
{
    /* Remove from the fd_list. */
    list_remove (&fd->elem);

    /* Close the file. */
    file_close (fd->file);

    /* Free resources. */
    free (fd);
}

/* -=| SYSCALLS DEFINITION |=- */ 

void 
halt (void) 
{
    power_off();
}

void 
exit (int status)
{
    struct thread *curr = thread_current ();
    struct PCB *pcb = NULL, *p = NULL;
    struct list_elem *e;

    /* Get our PCB */
    pcb = thread_current ()->pcb;

    ASSERT (pcb != NULL);

    /* Because we are exiting, delete all PCB of all children. We 
     * don't need to read exit_status anymore. */
    for (e = list_begin (&pcb->child_list); 
            e != list_end (&pcb->child_list); e = list_next (e))
    {
        p = list_entry (e, struct PCB, elem);
        ASSERT (p->parent_pcb == pcb);

        /* If the child process already exited, remove it's PCB. */ 
        if (p->status == PROC_EXITED)
        {
            lock_acquire (&p->hash_lock);
            list_remove (&p->elem);
            free (p);
        }
        else
            p->parent_pcb = NULL;
    }

    /* Lock the page table and the resource of this thread. */
    lock_acquire (&frame_list._lock);
    lock_acquire (&pcb->hash_lock);

    /* Fill the exit status. */
    pcb->exit_status = status;
    pcb->status = PROC_EXITED;

    /* Close all opened file descriptors. */
    empty_fdlist (pcb);

    /* Re-allow (or not) write on the executable file.*/
    lock_acquire (&file_sync);
        file_allow_write (pcb->exfile);
        file_close (pcb->exfile);
    lock_release (&file_sync);

    /* Free all the frames of the current process. */
    spage_flush_evicted (pcb);
    frame_flush_proc (&frame_list, curr);
    ASSERT (hash_empty (&pcb->frm_evicted));
    lock_release (&frame_list._lock);

    /* If we have a parent, wake it. */
    if (pcb->parent_pcb != NULL)
        sema_up (&pcb->sema);

    /* If not, simply remove the PCB from the list. */
    else
    {
        list_remove (&pcb->elem);
        free (pcb);
    }

    /* Verbose the system console. */
    printf ("%s: exit(%d)\n", curr->name, pcb->exit_status);
    
    /* Remove the page of the current process. */
    thread_exit();
}


pid_t 
exec (const char *file)
{
    /* Check that the file pointer is valid. */
    if (!check_ptr_validity ((void*)file, false))
        return -1;

    return process_execute(file);
}

int 
wait (pid_t pid)
{
    return process_wait (pid);
}

bool 
create (const char *file, unsigned initial_size)
{
    bool result_create;

    /* Check the 'file' pointer. */
    if (!check_ptr_validity ((void*)file, false))
        exit(-1);

    lock_acquire (&file_sync);
        result_create = filesys_create(file, initial_size);
    lock_release (&file_sync);

    return result_create;
}

bool 
remove (const char *file)
{
    bool res; 

    /* Check the 'file' pointer. */
    if (!check_ptr_validity ((void*)file, false))
        exit(-1);

    lock_acquire (&file_sync);
        res = filesys_remove(file);
    lock_release (&file_sync);

    return res;
}

int 
open (const char *file)
{
    struct PCB *pcb = thread_current ()->pcb;
    struct file *f;
    struct file_d *fd;

    ASSERT (pcb != NULL);

    /* Check the 'file' pointer. */
    if (!check_ptr_validity ((void*)file, false))
        exit(-1);

    /* Try to open the file. */
    lock_acquire (&file_sync);
    if ( (f = filesys_open (file)) == NULL)
    {
        lock_release (&file_sync);
        return -1;
    }

    /* File successfully opened. Create a new file descriptor. */
    fd = (struct file_d*) malloc (sizeof(struct file_d));
    if (fd == NULL)
    {
        lock_release (&file_sync);
        return -1;
    }

    /* Fill it. */
    fd->fd_no = pcb->fd_count++;
    fd->file = f;
    fd->pos = 0;
    fd->mmap_addr = NULL;

    /* Add it to the list of file descriptor in the PCB. */
    list_push_front (&pcb->fd_list, &fd->elem);
    lock_release (&file_sync);
    
    return fd->fd_no;
}

int 
filesize (int fd_no)
{
    int res;
    struct PCB *pcb = thread_current ()->pcb;
    struct file_d *fd;

    ASSERT (pcb != NULL);

    lock_acquire (&file_sync);
    /* Search the file descriptor in the PCB. Return if doesn't exist. */
    if ( (fd = process_search_fd (fd_no)) == NULL)
    {
        lock_release (&file_sync);
        return -1;
    }

    res = (int) file_length (fd->file);
    lock_release (&file_sync);

    return res;
}

int 
read (int fd_no, void *buffer, unsigned size)
{
    struct PCB *pcb = thread_current ()->pcb; 
    struct file_d *fd;
    unsigned i;
    int r_bytes = 0;
    uint8_t* sbuf = buffer;
    unsigned bytes_left = size;

    ASSERT (pcb != NULL);

    /* Check the buffer pointer, and check that the size of 
     * the buffer will not cause a page_fault. */
    if (!check_ptr_validity (buffer, true) || !check_ptr_validity (buffer+size, true))
        exit (-1);
    
    /* Lock the page where the buffer is located. */
    struct spage *pg = frame_pin (&frame_list, buffer);

    lock_acquire (&file_sync);

    /* Check if we want to read on the standard input. */
    if (fd_no == STDIN_FILENO)
    {
        for (i = 0; i < size; i++)
            ((uint8_t*) buffer)[i] = input_getc ();
        lock_release (&file_sync);
        frame_unpin (&frame_list, pg);
        return size;
    }

    /* Get the file descriptor. */
    if ( (fd = process_search_fd (fd_no)) == NULL)
    {
        lock_release (&file_sync);
        frame_unpin (&frame_list, pg);
        return -1;
    }
    ASSERT (fd->file != NULL);

    /* Restore previous file position. */
    file_seek (fd->file, fd->pos);

    /* Read. */
    while (bytes_left > 0)
    {
        /* Check if we will right above the current page. */
        if (pg_round_down (sbuf) != pg_round_down (sbuf + bytes_left))
        {
            /* The end of the buffer is not in the current page. Fetch the 
             * page if necessary. */
            check_ptr_validity (sbuf, true);
             
            /* Calculate the number of bytes we can read before reaching the 
             * end of the page, and read the calculated amount of bytes. */
            unsigned bytes_to_read = (uint8_t*) pg_round_up (sbuf+1) - sbuf;
            r_bytes += file_read (fd->file, sbuf, bytes_to_read);

            /* Update the number of bytes left, and unpin the used page. */
            bytes_left -= bytes_to_read;
            frame_unpin (&frame_list, pg);

            /* Move the sbuf pointer to the next page, and pin it. */
            sbuf = pg_round_up (sbuf+1);
            frame_unpin (&frame_list, pg);
        }
        else
        {
            /* The end of the buffer is in the current page. */
            r_bytes += file_read (fd->file, sbuf, bytes_left);
            bytes_left = 0;
        }
    }

    /* Save new position. */
    fd->pos = file_tell (fd->file);

    lock_release (&file_sync);
    frame_unpin (&frame_list, pg);

    return r_bytes;
}

int 
write (int fd_no, const void *buffer, unsigned size)
{
    int w_bytes;
    struct PCB *pcb = thread_current ()->pcb;
    struct file_d *fd;

    ASSERT (pcb != NULL);

    /* Check the buffer pointer, and check if the source buffer
     * doesn't read in forbidden memory space. */
    if (!check_ptr_validity ((void*) buffer, false)) 
        exit(-1); 
    if (!check_ptr_validity ((void*) buffer+size, false)) 
        exit(-1);

    /* Lock the page where the buffer is stored. */
    struct spage *pg = frame_pin (&frame_list, (uint8_t*)buffer);

    lock_acquire (&file_sync);

    /* Check if we want to write on the standard output. */
    if (fd_no == STDOUT_FILENO)
    {
        putbuf ((const char*) buffer, size);
        lock_release (&file_sync);
        frame_unpin (&frame_list, pg);
        return (int) size;
    }

    /* Get the file descriptor. */
    if ( (fd = process_search_fd (fd_no)) == NULL)
    {
        lock_release (&file_sync);
        frame_unpin (&frame_list, pg);
        return -1;
    }
    ASSERT (fd->file != NULL);

    /* Restore previous position. */
    file_seek (fd->file, fd->pos);

    /* Write. */
    w_bytes = file_write (fd->file, buffer, size);

    /* Save new position. */
    fd->pos = file_tell (fd->file);

    lock_release (&file_sync);
    frame_unpin (&frame_list, pg);

    return w_bytes;
}

void 
seek (int fd_no, unsigned position)
{
    struct file_d *fd;
    lock_acquire (&file_sync);

    /* Search the file descriptor. */
    if ( (fd = process_search_fd (fd_no)) == NULL)
    {
        lock_release (&file_sync);
        return;
    }

    /* Change the current position in the file. */
    fd->pos = (off_t) position;
    lock_release (&file_sync);
}

unsigned 
tell (int fd_no)
{
    unsigned res;
    struct file_d *fd;

    lock_acquire (&file_sync);
    /* Search the file descriptor. */
    if ( (fd = process_search_fd (fd_no)) == NULL)
    {
        lock_release (&file_sync);
        return -1;
    }
    /* Get the position in the file. */
    res = (unsigned) fd->pos;
    lock_release (&file_sync);

    return res;
}

void 
close (int fd_no)
{
    struct PCB *pcb = thread_current ()->pcb;
    struct file_d *fd;

    ASSERT (pcb != NULL);

    lock_acquire (&file_sync);
    /* Search the file descriptor in the PCB. Return if doesn't. */
    if ( (fd = process_search_fd (fd_no)) == NULL)
    {
        lock_release (&file_sync);
        return;
    }

    /* Remove from the fd_list. */
    list_remove (&fd->elem);

    /* Close the file descriptor. */
    file_close (fd->file);
    lock_release (&file_sync);

    /* Free the file descriptor. */
    free (fd);
}

mapid_t
mmap (int fd_no, void *addr)
{
    struct file_d *fd;
    struct thread *curr = thread_current ();
    struct PCB *pcb = curr->pcb;
    struct spage *pg;
    uint8_t *addr_end, *sbuf = addr, flag = 0x00;
    uint32_t zeros;
    off_t bytes_left, ofs;
    disk_sector_t sec;

    /* Check if the address is page aligned. */
    if (pg_round_down (addr) != addr)
        return -1;

    /* Check if the file descriptor exist in the PCB. If fd_no represent
     * FILENO_STDIN or FILENO_STDOUT, it will automatically fail. */
    if ((fd = process_search_fd (fd_no)) == NULL)
        return -1;

    /* Check that the size of the file is correct (!= 0). */
    if ( (bytes_left = (unsigned) file_length(fd->file)) == 0)
        return -1;

    /* Calculate the top address that will be occupied by the mapped file. */
    lock_acquire (&file_sync);
        addr_end = addr + file_length (fd->file);
    lock_release (&file_sync);
    
    /* Check that this address is not in kernel space, not on the stack, and 
     * not null. */
    if (is_kernel_vaddr (addr_end) || addr == NULL || 
            addr_end >= (uint8_t*) STACK_LIM)
        return -1;

    /* Check that the mapping addresses are not oocupied by another frame. */
    if (spage_lookup (&pcb->frm_evicted, addr) != NULL)
        return -1;

    /* Check if the address is already occupied by another page. */
    if (lookup_page (curr->pagedir, addr, false) != NULL)
        return -1;
    if (lookup_page (curr->pagedir, pg_round_down(addr_end), false) != NULL)
        return -1;

    /* Parameters has been checked. We can now map the file. */
    /* Reopen the file to prevent deleting. */
    mapid_t mapid = mapfd_create (fd, addr);

    /* All the mmaped spages should be loaded lazily. */
    flag = LOC_LAZY | LOC_EVICTED | FRM_MMAP;
    ofs = 0;

    /* Get the starting block of the page in the file to map. */
    sec = byte_to_sector (file_get_inode (fd->file), ofs);

    /* Populate the memory with mmaped spages. */
    while (bytes_left > 0)
    {
        /* Calculate the number of zeros to add at the end of the page. */
        zeros = bytes_left > PGSIZE ? 0 : PGSIZE-bytes_left;
        pg = spage_allocate (sbuf, flag, sec, zeros);
        pg->mm_no = mapid;

        /* Advance. */
        sbuf += PGSIZE;
        sec += PGSIZE/DISK_SECTOR_SIZE;
        bytes_left -= PGSIZE;
    }

    return mapid;
}

void
munmap (mapid_t mapid)
{
    struct PCB *pcb = thread_current ()->pcb;
    struct file_d *fd;
    struct spage *pg;
    unsigned size;
    int i, pg_nb;

    /* Check if the mapid is valid. */
    fd = process_search_fd (mapid);
    if ( (fd == NULL) || (fd->mmap_addr == NULL)) 
        return;

    /* Indicate to the action_invalidate_mmap function which mapid we want 
     * to remove from the memory. The pointer to mm_no_aux has been provided
     * at the initialization of the hash table as an auxiliary data. */
    pcb->mm_no_aux = mapid;

    /* Invalidate all the hash element that point to a spage corresponding to
     * the given mapid. Next time a page fault will occurs to the address, the 
     * frame will be automatically freed. */
    hash_apply (&pcb->frm_evicted, action_invalidate_mmap);

    /* Scan the list of system-wide frame and remove all the frames
     * with the corresponding mapid. */
    lock_acquire (&frame_list._lock);

    /* Calculate the number of page occupied by the mmaped file. */
    size = file_length (fd->file);
    pg_nb = size/PGSIZE + ((size%PGSIZE) > 0 ? 1 : 0);

    /* Remove the frames one by one. */
    for (i=0; i < pg_nb; i++)
    {
        pg = frame_lookup (&frame_list, pcb, fd->mmap_addr + i*PGSIZE);

        if (pg != NULL)
        {
            /* Remove the frame from the frame list. */
            frame_remove (&frame_list, pg);

            /* Copy on write. */
            ASSERT (pg->flag & FRM_MMAP);
            swap_evict_spage (pg);
            palloc_free_page (pg->kaddr);
            frame_uninstall (pg);
            free (pg);
        }
    }
    lock_release (&frame_list._lock);

    /* Free resources. */
    mapfd_close (fd);
}


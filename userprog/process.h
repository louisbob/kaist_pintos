#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include <list.h>

#include "lib/kernel/hash.h"
#include "lib/user/syscall.h"
#include "filesys/off_t.h"
#include "threads/thread.h"
#include "threads/synch.h"

/* File descriptor structure. */
struct file_d
{
    int fd_no;              /* File decriptor number. */
    void* mmap_addr;        /* Starting address for mapping. */
    off_t pos;              /* Position in the file. */
    struct file *file;      /* File structure. */
    struct list_elem elem;  /* List element. */
};

/* Status of process. */
enum process_status
{
    PROC_ERROR,
    PROC_LOADED,
    PROC_LIVING,
    PROC_WAITING,
    PROC_EXITED
};

/* Process Control Block structure. Each PROC_LIVING process should have a 
 * valid PCB. */
struct PCB
{
    pid_t pid;                  /* Process identifier. */
    struct PCB* parent_pcb;     /* PCB of the parent process. */

    struct file *exfile;        /* Pointer to the file beeing executed. */
    enum process_status status; /* Current status of the process. */
    int exit_status;            /* Exit number of the process. */
    int fd_count;               /* File descriptor allocation value. */
    mapid_t mm_no_aux;           /* Auxiliary data for action hash. */

    struct hash frm_evicted;    /* All frame owned by the process. */
    struct lock hash_lock;      /* Ensure synchronization. */

    struct list fd_list;        /* List of all opened file descriptors. */
    struct list child_list;     /* List of all child processes. */
    struct list_elem elem;      /* List element. */

    struct semaphore sema;      /* Synchronization with parent process. */
    struct semaphore load;      /* File load synchronization. */
    struct semaphore run;       /* Run thread synchronization. */
};

tid_t process_execute (const char *file_name);
bool install_page (void *upage, void *kpage, bool writable);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);
void process_init (void);
void process_kill (struct PCB *parent);

#endif /* userprog/process.h */

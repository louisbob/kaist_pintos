#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H
#include "lib/user/syscall.h" 
#include "lib/syscall-nr.h"
#include "threads/synch.h"
#include "userprog/process.h"

void syscall_init (void);
struct file_d* process_search_fd (int fd);

/* Projects 2 syscalls. */
void halt (void) NO_RETURN;
void exit (int status) NO_RETURN;
pid_t exec (const char *file);
int wait (pid_t);
bool create (const char *file, unsigned initial_size);
bool remove (const char *file);
int open (const char *file);
int filesize (int fd_no);
int read (int fd_no, void *buffer, unsigned length);
int write (int fd_no, const void *buffer, unsigned length);
void seek (int fd_no, unsigned position);
unsigned tell (int fd_no);
void close (int fd_no);

/* Project 3 syscalls. */
void munmap (mapid_t mapid);
mapid_t mmap (int fd_no, void *addr);

#endif /* userprog/syscall.h */

#include <debug.h>
#include <stdio.h>
#include <round.h>
#include "filesys/file.h"
#include "lib/kernel/hash.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "userprog/pagedir.h"
#include "userprog/process.h"
#include "userprog/syscall.h"
#include "vm/frame.h"
#include "vm/spage.h"
#include "vm/swap.h"

static void spage_install (struct spage *pg, bool lazy);
static void spage_evict (struct spage *victim);
static struct spage* frame_get_victim (void);
static void action_destroy_frame (struct hash_elem *elem, void *aux);
static bool check_access(struct spage *pg);

/* Store the frames currently in use on the system. */
struct frame_hl frame_list;

/* Point to the next element to check in the eviction algorithm. */
struct spage *evict_pos;

/* HASH functions. */
unsigned spage_hash (const struct hash_elem *p_, void *aux UNUSED);
bool spage_less (const struct hash_elem *a_, const struct hash_elem *b_,
                void *aux UNUSED);

/* Initialize a frame table. Frame table can be a hash-list (system-wide)
 * or a hash table (process-wide). If NULL is provided in parameter, this
 * function initialize the system-wide frame list. If not NULL, frame_init()
 * initialize the frame table of the given PCB. */
void 
spage_init (struct PCB *pcb)
{
    if (pcb == NULL)
    {
        lock_init (&frame_list._lock);
        frame_init (&frame_list);
    }
    else  /* Init the hash table. */
    {
        lock_init (&pcb->hash_lock);
        hash_init (&pcb->frm_evicted, spage_hash, spage_less, &pcb->mm_no_aux);
    }
}

/* Allocate a new frame from the user pool, and add it to the list of
 * used frame on the system. If no frame are available, the system use
 * the eviction policy to free a frame. If the flag LAZY is present,
 * no frame are retrieved from the user pool, and the created spage will
 * be stored in the hash of evicted frame of the calling process. */
struct spage* 
spage_allocate (uint8_t *vaddr,  uint8_t flag, disk_sector_t sec, 
        uint32_t page_zeros)
{
    struct thread *curr_t = thread_current ();
    uint8_t *kaddr = NULL;
    struct spage *pg;

    /* If we load lazily, we don't need to get a frame from the pool. */
    if ( !(flag & LOC_LAZY))
    {
        /* Get a new frame. */
        kaddr = palloc_get_page (PAL_USER | PAL_ZERO);

        /* Check if we have enough frames. */
        while (kaddr == NULL)
        {
            /* If not, try to evict a frame. */
            spage_evict (frame_get_victim ());

            /* Try to allocate again. */
            kaddr = palloc_get_page (PAL_USER);
        }
    }
    else
        ASSERT ( !(flag & FRM_STACK));

    /* Create a new spage element. */
    if ( (pg = malloc (sizeof (struct spage))) == NULL)
        PANIC ("Can't malloc() a new spage.");
    
    /* Fill it. */
    pg->flag = flag;
    pg->owner = curr_t;
    pg->vaddr = vaddr;
    pg->kaddr = kaddr;
    pg->pinned = false;
    pg->pzeros = page_zeros;
    pg->disk_sec = sec;

    /* Install the page. */
    spage_install (pg, flag & LOC_LAZY ? true : false);

    return pg;
}

/* Install a new page in the current process. If lazy is true, the page is not 
 * installed in the page table of the process, and is not added to the 
 * system-wide page list but in the list of evicted frame of the current 
 * process.*/
void
spage_install (struct spage *pg, bool lazy)
{
    struct PCB *pcb = pg->owner->pcb;

    /* If the frame need to be loaded lazily, put it directly in the list of
     * evicted frame of the current process. */
    if (lazy)
    {
        lock_acquire (&pcb->hash_lock);
            /* Add the frame to the list of evicted frames. */
            hash_insert (&pcb->frm_evicted, &pg->hash_elem);
        lock_release (&pcb->hash_lock);
    }
    else
    {
        ASSERT (pg->kaddr != NULL);
        lock_acquire (&frame_list._lock);

            /* If the list of frame is empty, set the eviction position to this
             * frame, because it's the first frame in the list. */
            if (frame_empty (&frame_list))
            {
                frame_push_front (&frame_list, pg);
                evict_pos = pg;
            }
            else /* Insert just before the next eviction's candidate page. */
                frame_insert (&frame_list, evict_pos, pg);

            /* Install new page in the page directory of the current process. */
            install_page (pg->vaddr, pg->kaddr, !(pg->flag & FRM_SEGRO));

        lock_release (&frame_list._lock);
    }
}

/* Check if the specified address match with a spage that has been previously
 * evicted by the system. If yes, the corresponding spage is restored. If not, 
 * simply return false, indicating that the virtual address doesn't correspond 
 * to any valid virtual address. */
bool
spage_fetch_evicted (void* vaddr)
{
    struct PCB *pcb = thread_current ()->pcb;
    struct spage *pg;
    void *kaddr;
    
    /* Get the frame from the address in the hash table. */
    pg = spage_lookup (&pcb->frm_evicted, pg_round_down(vaddr));

    if (pg == NULL) return false;

    /* If the page is pinned, it means that it is currently being evicted.
     * Wait for the end of the eviction. */
    while (pg->pinned);
    pg->pinned = true;

    /* Check if the page should be removed (case of mmaped file.) */
    if (pg->flag & LOC_UNMAPPED)
    {
        lock_acquire (&pcb->hash_lock);
            hash_delete (&pcb->frm_evicted, &pg->hash_elem);
        lock_release (&pcb->hash_lock);
        return false;
    }

    /* Get a new page to store data. */
    kaddr = palloc_get_page (PAL_USER | PAL_ZERO);

    /* Check we got a frame. */
    while (kaddr == NULL)
    {
        spage_evict (frame_get_victim ());
        kaddr = palloc_get_page (PAL_USER);
    }

    lock_acquire (&pcb->hash_lock);
      pg->kaddr = kaddr;

      /* Fetch the segment from the disk or the swap. */
      swap_retrieve_spage (pg);

      /* Remove the frame from the evicted hash table. */
      hash_delete (&pcb->frm_evicted, &pg->hash_elem);

      /* Set the page as accessed to avoid it to be evicted quickly. */
      pagedir_set_accessed (pg->owner->pagedir, pg->vaddr, true);
      
    lock_release (&pcb->hash_lock);

    /* Re-insert the page in the in-memory page list. */
    spage_install (pg, false);

    pg->pinned = false;

    return true;
}

/* Evict the spage provided in parameter from the system-wide frame list. 
 * If the spage contain a writable segment, it will be evicted to the swap.
 * If the spage contain R/O segment, it will be simply erased, as we can reload
 * it again from the binary file. 
 * If the spage is a mapped file, modification will be written back to the file.
 * */
static void 
spage_evict (struct spage *victim)
{
    struct PCB *pcb = victim->owner->pcb;

    ASSERT(lock_held_by_current_thread (&pcb->hash_lock));
    
    /* Pin the page. */
    while (victim->pinned);
        victim->pinned = true;

    /* Evict the spage to the swap, or just change its attributes if it
     * is a R/O segment. */
    swap_evict_spage (victim);

    /* Remove the frame from the memory. */
    palloc_free_page (victim->kaddr);
    victim->kaddr = NULL;

    /* Add the frame to the list of evicted frames. */
    hash_insert (&pcb->frm_evicted, &victim->hash_elem);

    

    victim->pinned = false;

    lock_release (&pcb->hash_lock);
}

/* When the memory is full, this function is called to choose the best frame to
 * evict. It uses the second chance algorithm. 
 * Return the address of the evicted page. */
static struct spage* 
frame_get_victim (void)
{
    struct spage *victim = evict_pos;
    struct spage *pg;

    lock_acquire (&frame_list._lock);
    /* We scan all the pages in memory and try to find one that is not pinned,
     * nor belong to a process that is dying, nor that the hash_lock is locked. */
    while (true)
    {
        pg = victim;
        if(!pg->pinned)
            if (pg->owner->pcb->status != PROC_EXITED)
                if(lock_try_acquire(&pg->owner->pcb->hash_lock))
                {
                    /* Check the access bit and give a second chance if 
                     * needed. */
                    if(check_access(pg))
                        break;
                    else
                        lock_release (&pg->owner->pcb->hash_lock);
                }

        /* Read the next spage in memory. */
        victim = frame_next (&frame_list, victim);
    }

    /* Update the evict_pos. */
    evict_pos = frame_next (&frame_list, victim);

    /* Remove the victim from the in-memory pages. */
    frame_remove (&frame_list, victim);

    /* Update the page directory. */
    frame_uninstall (victim);

    lock_release (&frame_list._lock);

    return victim;
}

/* Check whether or not the specified page has been accessed. If yes, give
 * a second chance by resetting the access bit in the PTE. */
bool check_access(struct spage *pg)
{
     uint32_t *pdir = pg->owner->pagedir;

     if (pagedir_is_accessed (pdir, pg->vaddr))
     {
         /* Give a second chance. */
         pagedir_set_accessed (pdir, pg->vaddr, false);
         return false;
     }
     return true;
}

/* Remove all spages that have been evicted, especially for freeing resources 
 * on the swap disk when process exit. */
void 
spage_flush_evicted (struct PCB *pcb)
{
    /* We should have the lock on the frm_evicted spage hash table. */
    ASSERT(lock_held_by_current_thread (&pcb->hash_lock));

    if (!hash_empty (&pcb->frm_evicted))
        hash_clear (&pcb->frm_evicted, action_destroy_frame );
}

/* Remove all the frames from the memory corresponding to the specified thread.
 * If on of the frame is mmaped, the content is written back to the disk. */
void
frame_flush_proc (struct frame_hl *fhl, struct thread *t)
{
    uint32_t *vaddr, *sav_pde = NULL, *sav_pte = NULL;
    struct spage *pg;

    /* Browse the pagedir of the current process, and uninstall pages each time. */
    do
    {
        /* Seek into the page table of the process. */
        vaddr = pagedir_browse(t->pagedir, &sav_pde, &sav_pte);

        /* Get the page in the hash table. */
        pg = frame_lookup (fhl, t->pcb, vaddr);
        if (pg == NULL) break;

        /* Move the evict_pos forward if we are trying to delete the page
         *          * pointed by it. */
        if (pg == evict_pos)
            evict_pos = frame_next (fhl, evict_pos);

        /* Remove the frame from the hashlist. */
        frame_remove (fhl, pg);

        /* Copy on write if the correspond to a mapped file. */
        if (pg->flag & FRM_MMAP)
            swap_evict_spage (pg);

        /* Free the page. */
        palloc_free_page (pg->kaddr);
        frame_uninstall (pg);
        free (pg);
    }
    while (vaddr != NULL);
}

/* Remove a mapping of the user virtual address space of the process that
 * own the provided frame. */
void
frame_uninstall (struct spage *frm)
{
    pagedir_clear_page (frm->owner->pagedir, frm->vaddr);
}

/* Search the provided virtual address in the hash table provided in 
 * parameters. */
struct spage *
spage_lookup (struct hash *frm_evicted, void *vaddr)
{
    struct spage pg;
    struct hash_elem *e;

    pg.vaddr = vaddr;
    e = hash_find (frm_evicted, &pg.hash_elem);

    return e != NULL ? hash_entry (e, struct spage, hash_elem) : NULL;
}

/* Invalidate spages that correspond to a mapped file. Then, on the next 
 * page-fault on the marked spages, the page fetcher will automatically delete
 * the page, and will fail to retrieve it. */
void
action_invalidate_mmap (struct hash_elem *elem, void *mm_no_aux)
{
    struct spage *pg = hash_entry (elem, struct spage, hash_elem);

    if (pg->mm_no == *(mapid_t*)mm_no_aux)
        pg->flag |= LOC_UNMAPPED;
}

/* Action function for the hash table that remove all spage of a process
 * from the swap. */
void 
action_destroy_frame (struct hash_elem *elem, void *aux)
{
    struct spage *pg = hash_entry (elem, struct spage, hash_elem);
     
    /* Check if on stack or W/R segment. */
    if (!(pg->flag & FRM_SEGRO))
    {
        /* If yes, free the corresponding element on the swap. */
        swap_erase_spage (pg);
        free (pg);
    }
}

/* Spage hash function. */
unsigned
spage_hash (const struct hash_elem *p_, void *aux UNUSED)
{
    const struct spage *p = hash_entry (p_, struct spage, hash_elem);
    return hash_bytes (&p->vaddr, sizeof p->vaddr);
}

/* Spage hash comparison function. */
bool
spage_less (const struct hash_elem *a_, const struct hash_elem *b_,
        void *aux UNUSED)
{
    const struct spage *a = hash_entry (a_, struct spage, hash_elem);
    const struct spage *b = hash_entry (b_, struct spage, hash_elem);
    return a->vaddr < b->vaddr;
}



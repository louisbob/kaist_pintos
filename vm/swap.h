#ifndef VM_SWAP_H
#define VM_SWAP_H

#include "vm/spage.h"

bool swap_init (void);
void swap_evict_spage (struct spage *pg);
void swap_retrieve_spage (struct spage *pg);
void swap_erase_spage (struct spage *pg);

#endif

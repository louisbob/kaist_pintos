#ifndef VM_SPAGE_H
#define VM_SPAGE_H
#include "devices/disk.h"
#include "lib/kernel/hash.h"
#include "filesys/off_t.h"
#include "userprog/process.h"
#include "threads/synch.h"

#define CHKBIT(var,pos) ((var) & (1<<(pos)))

#define eeDEBUG
#ifdef DEBUG
 #define D 
#else
 #define D for(;0;)
#endif

enum spage_type
{
    FRM_SEGRO = 0x01,  /* Read-only segment. */
    FRM_SEGWR = 0x02,  /* Writeable segment. */
    FRM_STACK = 0x04,  /* Stack frame. */
    FRM_MMAP = 0x08    /* Memory mapped frame. */ 
};

enum spage_attribute
{
    LOC_MEM = 0x10,        /* The frame is in the memory. */
    LOC_EVICTED = 0x20,    /* The frame is on the swap. */
    LOC_LAZY = 0x40,       /* Load the frame lazily. */
    LOC_UNMAPPED = 0x80    /* The frame has been unmapped. */
};

struct spage 
{
    uint8_t flag;               /* Type of the spage. */   
    struct thread *owner;       /* Thread struct of the owner of the spage. */
    void *vaddr;                /* Virtual address. */
    void *kaddr;                /* Address in virtual kernel space. */
    bool pinned;                /* Indicate whether or not a page is pinned. */
    mapid_t mm_no;              /* Store the mapid of the spage. */

    /* Manage segment loading. */
    uint32_t pzeros;            /* Number of zeros at the end of spage. */
    disk_sector_t disk_sec;     /* Store the swap or disk sector of spage. */

    struct hash_elem hash_elem; /* Hash table element. */
    struct list_elem elem;      /* List element. */
};

#include "vm/frame.h"

void spage_init (struct PCB *pcb);
struct spage *spage_allocate (uint8_t *vaddr, uint8_t flag, 
        disk_sector_t sec, uint32_t page_zeros);
bool spage_fetch_evicted (void* vaddr);
void spage_flush_evicted (struct PCB *pcb);
void spage_flush_mmap (struct frame_hl *fhl, struct PCB *pcb, struct file_d *fd);
void frame_flush_proc (struct frame_hl *fhl, struct thread *t);
void frame_uninstall (struct spage *frm);
struct spage *spage_lookup (struct hash *spg_evicted, void *vaddr);
void action_invalidate_mmap (struct hash_elem *elem, void *mm_no_aux);

#endif

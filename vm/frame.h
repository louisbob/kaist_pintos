#ifndef VM_FRAME_H
#define VM_FRAME_H
#include "lib/kernel/hash.h"
#include "lib/kernel/list.h"
#include "threads/thread.h"
#include "threads/synch.h"

/* Hybrid data structure that regroup a hash table with a chained list. 
 * It also provide some synchronization. */
struct frame_hl
{
    struct list _list;      /* Hash table. */
    struct hash _hashtab;   /* List of element frame. */
    struct lock _lock;      /* Synchronization. */
};

#include "vm/spage.h"

void frame_init (struct frame_hl *fhl);
bool frame_empty (struct frame_hl *fhl);
void frame_push_front (struct frame_hl *fhl, struct spage *pg);
void frame_insert (struct frame_hl *fhl, struct spage *before, struct spage *pg);
struct spage *frame_remove (struct frame_hl *fhl, struct spage *pg);
struct spage *frame_next (struct frame_hl *fhl, struct spage *pg);
struct spage *frame_lookup (struct frame_hl *fhl, struct PCB *pcb, void* vaddr);
struct spage* frame_pin (struct frame_hl *fhl, uint8_t *vaddr);
void frame_unpin (struct frame_hl *fhl, struct spage *pg);

#endif

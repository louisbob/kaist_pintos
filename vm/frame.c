#include <string.h>
#include <stdio.h>
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/vaddr.h"
#include "userprog/pagedir.h"
#include "vm/frame.h"
#include "vm/swap.h"

/* Frame hash function. */
static unsigned
frame_hash (const struct hash_elem *p_, void *aux UNUSED)
{
    uint32_t h;
    const struct spage *p = hash_entry (p_, struct spage, hash_elem);

    /* Hash the PID and the virtual address. The first 3 bytes of each virtual
     * page address are not used. So we use them to store the PID. It's similar
     * to a inverted page table. In bring a limitation of 2^3 processes running
     * at the same time on the system. */
    h = p->owner->pcb->pid & 0xFFF;
    h |= ((uint32_t)p->vaddr);  

    return hash_bytes (&h, sizeof (h));
}

/* Frame hash comparison function. */
static bool
frame_less (const struct hash_elem *a_, const struct hash_elem *b_,
        void *aux UNUSED)
{
    const struct spage *a = hash_entry (a_, struct spage, hash_elem);
    const struct spage *b = hash_entry (b_, struct spage, hash_elem);

    uint32_t h1, h2;
    h1 = (a->owner->pcb->pid & 0xFFF) | ((uint32_t)a->vaddr);
    h2 = (b->owner->pcb->pid & 0xFFF) | ((uint32_t)b->vaddr);

    return h1 < h2;
}

/* Initialization of the hybrid hash-list. As all the following functions
 * are analogous to list and hash functions, they are not commented. */
void 
frame_init (struct frame_hl *fhl)
{
    list_init (&fhl->_list);
    hash_init (&fhl->_hashtab, frame_hash, frame_less, NULL);
    lock_init (&fhl->_lock);
}

bool 
frame_empty (struct frame_hl *fhl)
{
    return list_empty (&fhl->_list);
}

void 
frame_push_front (struct frame_hl *fhl, struct spage *pg)
{
    list_push_front (&fhl->_list, &pg->elem);
    hash_insert (&fhl->_hashtab, &pg->hash_elem);
}

void 
frame_insert (struct frame_hl *fhl, struct spage *before, struct spage *pg)
{
    list_insert (&before->elem, &pg->elem);
    hash_insert (&fhl->_hashtab, &pg->hash_elem);
}

struct spage*
frame_remove (struct frame_hl *fhl, struct spage *pg)
{
    struct list_elem *e;
    e = list_remove (&pg->elem);
    hash_delete (&fhl->_hashtab ,&pg->hash_elem);

    return list_entry (e, struct spage, elem);
}

/* Return the next element. As the hashlist is circular, this function
 * return the first element when the end of the list is reached. */
struct spage*
frame_next (struct frame_hl *fhl, struct spage *pg)
{
    struct list_elem *e;
    ASSERT (lock_held_by_current_thread (&fhl->_lock));
    ASSERT (!list_empty (&fhl->_list))

    if (&pg->elem == list_back (&fhl->_list))
        e = list_begin (&fhl->_list);
    else
        e = list_next(&pg->elem);

    return list_entry (e, struct spage, elem);
}

/* Use the power of the hash table to retrieve the frame corresponding
 * to the provided PCB and vaddr. Return NULL if nothing has been found. 
 * Vaddr must be page aligned. */
struct spage*
frame_lookup (struct frame_hl *fhl, struct PCB *pcb, void* vaddr)
{
    struct spage pg;
    struct thread t;
    struct hash_elem *e;

    /* Check that the vaddr is page aligned. */
    ASSERT (pg_round_down (vaddr) == vaddr);

    t.pcb = pcb;
    pg.owner = &t;
    pg.vaddr = vaddr;

    e = hash_find (&fhl->_hashtab, &pg.hash_elem);

    return e != NULL ? hash_entry (e, struct spage, hash_elem) : NULL;
}

/* Pin a frame at the address 'vaddr', so it can't be evicted from 
 * the memory. */
struct spage*
frame_pin (struct frame_hl *fhl, uint8_t *vaddr)
{
    struct PCB *pcb = thread_current ()->pcb;
    struct spage *pg;

    lock_acquire (&fhl->_lock);
        pg = frame_lookup (fhl, pcb, pg_round_down(vaddr));
        ASSERT (pg != NULL);
        pg->pinned = true;
    lock_release (&fhl->_lock);
    
    return pg;
}

/* Unpin the given frame. */
void
frame_unpin (struct frame_hl *fhl, struct spage *pg)
{
    lock_acquire (&fhl->_lock);
        pg->pinned = false;
    lock_release (&fhl->_lock);
}

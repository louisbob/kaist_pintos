#include <debug.h>
#include <stdio.h>
#include <string.h>
#include "devices/disk.h"
#include "lib/kernel/bitmap.h"
#include "userprog/pagedir.h"
#include "vm/spage.h"
#include "vm/swap.h"
#include "threads/synch.h"
#include "threads/vaddr.h"
#include "threads/thread.h"

/* SWAP disk currently attached to Pintos. */
static struct disk *swap_disk;
static struct disk *main_disk;

/* Size of the swap in sector. */
static disk_sector_t swap_disk_size;

/* Bitmap of free swap slots. */
static struct bitmap *swap_map;

/* Synchronization for the swap. */
struct lock swap_lock;

/* Initialize the SWAP to support frame eviction. */
bool 
swap_init (void)
{
    /* Identify the swap disk. */
    swap_disk = disk_get (1, 1);
    main_disk = disk_get (0, 1);

    if (swap_disk == NULL)
        return false;

    /* Get the size of the current SWAP disk. */
    swap_disk_size = disk_size (swap_disk);

    /* Init the map of used swap slots. */
    swap_map = bitmap_create (swap_disk_size);
    ASSERT (swap_map != NULL);

    /* Init the synchronization lock. */
    lock_init (&swap_lock);

    return true;
}

/* Transfer the given spage to the swap disk, and store the necessary 
 * information to retrieve it if needed. */
void 
swap_evict_spage (struct spage *pg)
{
    size_t sec_idx, cnt;

    ASSERT (pg->flag & LOC_MEM);
    ASSERT (!(pg->flag & LOC_EVICTED));
    ASSERT (!(pg->flag & LOC_LAZY));

    /* Update the frame info. */
    pg->flag &= 0x0F;
    pg->flag |= LOC_EVICTED;

    /* If the spage correspond to a RO segment, we don't need to copy it on
     * the swap. */
    if (pg->flag & FRM_SEGRO)
        return;

    /* If the spage correspond to a mmaped frame, check if it has been written,
     * and if yes, copy on the disk. */
    if (pg->flag & FRM_MMAP)
    {
        /* Check if the spage has been written. */
        if (pagedir_is_dirty (pg->owner->pagedir, pg->vaddr))
        {
            /* Set the number of zeros at the end of the buffer. */
            uint8_t *vzero = (pg->kaddr + PGSIZE) - pg->pzeros;
            memset (vzero, 0, pg->pzeros);

            uint8_t *sbuf = pg->vaddr;
            unsigned bytes_left = PGSIZE - pg->pzeros; 
            sec_idx = pg->disk_sec;

            /* We need to write back the modification to the disk, because the
             * page is dirty.*/
            while (bytes_left > 0)
            {
                disk_write (main_disk, sec_idx, sbuf);
                sbuf += DISK_SECTOR_SIZE;
                sec_idx++;

                if (bytes_left < DISK_SECTOR_SIZE) 
                    bytes_left = 0;
                else 
                    bytes_left -= DISK_SECTOR_SIZE;
            }
            return;
        }
        else return;
    }

    /* The frame is not a R/O segment nor a mmaped file. */
    lock_acquire (&swap_lock);

    /* Scan the bitmap and search for enough consecutive sector to store a 
     * frame. */
    sec_idx = bitmap_scan_and_flip (swap_map, 0, PGSIZE/DISK_SECTOR_SIZE, false);
    if (sec_idx == BITMAP_ERROR) 
        PANIC ("SWAP disk is full!");

    /* Save the sector where we copied the frame. */
    pg->disk_sec = sec_idx;
    cnt = 0;

    /* Write the frame to the swap. */
    while (sec_idx < (pg->disk_sec + PGSIZE/DISK_SECTOR_SIZE))
    {
        disk_write (swap_disk, sec_idx, pg->kaddr + cnt * DISK_SECTOR_SIZE);
        cnt++;
        sec_idx++;
    }

    lock_release (&swap_lock);
}

/* Retrieve the given spage from the swap, and 'zeros' the sectors where the 
 * spage has been stored for security reason. If the spage is pointing to 
 * a R/O segment or a mapped file, it loads it from the hard disk. */
void 
swap_retrieve_spage (struct spage *pg)
{
    size_t cnt, sec_idx;
    uint8_t zeros[DISK_SECTOR_SIZE] = {0};

    /* Check if the spage hasn't been allocated previously. */
    ASSERT (pg->kaddr != NULL);

    ASSERT (pg->flag & LOC_EVICTED);
    cnt = 0;
    sec_idx = pg->disk_sec;
    lock_acquire (&swap_lock);

    /* Check whether or not we need to load the spage from disk or swap. */
    if (!(pg->flag & FRM_SEGRO) && !(pg->flag & FRM_MMAP) && !(pg->flag & LOC_LAZY))
    {
        /* Read the spage from the swap, and 'zeros' the corresponding 
         * sectors. */
        while (sec_idx < (pg->disk_sec + PGSIZE/DISK_SECTOR_SIZE))
        {
            disk_read (swap_disk, sec_idx, pg->kaddr + cnt * DISK_SECTOR_SIZE);
            disk_write (swap_disk, sec_idx, zeros);
            cnt++;
            sec_idx++;
        }

        /* Free the swap slots. */
        bitmap_set_multiple (swap_map, pg->disk_sec, PGSIZE/DISK_SECTOR_SIZE, false);
    }
    else
    {
        while (sec_idx < (pg->disk_sec + PGSIZE/DISK_SECTOR_SIZE))
        {
            disk_read (main_disk, sec_idx, pg->kaddr + cnt * DISK_SECTOR_SIZE);
            cnt++;
            sec_idx++;
        }

        /* Add zeros because we are loading a segment. */
        uint8_t *vzero = (pg->kaddr + PGSIZE) - pg->pzeros;
        memset (vzero, 0, pg->pzeros );
    }

    /* Update frame info. Reset LAZY flag if necessary. */
    pg->flag &= 0x0F;
    pg->flag |= LOC_MEM;

    lock_release (&swap_lock);
}

/* Remove the given spage from the swap disk. */
void
swap_erase_spage (struct spage *pg)
{
    size_t cnt, sec_idx;
    uint8_t zeros[DISK_SECTOR_SIZE] = {0};

    ASSERT (pg->flag & LOC_EVICTED);

    cnt = 0;
    sec_idx = pg->disk_sec;
    lock_acquire (&swap_lock);

    /* Zero sectors where the page has been stored, for security. */
    while (sec_idx < (pg->disk_sec + PGSIZE/DISK_SECTOR_SIZE))
    {
        disk_write (swap_disk, sec_idx, zeros);
        cnt++;
        sec_idx++;
    }

    /* Free the swap slots. */
    bitmap_set_multiple (swap_map, pg->disk_sec, PGSIZE/DISK_SECTOR_SIZE, false);
    lock_release (&swap_lock);
}

